# 管理ツールにログインする
##1 下記URLにアクセス
http://fsaki.jp/wp-admin/

##2 ユーザー名とパスワードを入力
![](http://i.gyazo.com/f7efa7a7ac277ecfec29a67118daf7c2.png)

##3 ログイン
ログインすると管理画面が表示される。

![](http://i.gyazo.com/f94d57a6731fac8e7e4bc1f559974ccc.png)


# ブログの記事を投稿する
##1 メニューの投稿 を選ぶ
![](http://i.gyazo.com/7ce818242ef31e94413c5f6440a9684e.png)

##2 投稿管理画面で「新規追加」ボタンを押す
![](http://i.gyazo.com/3a4f1d293aca15555032b15d57b090b3.png)

##3 記事を編集して投稿する
![](http://i.gyazo.com/1f53e0b8ae4337985ae40e43ee60f724.png)

##4 [補足] 画像のアップロードについて
###i) 記事編集画面で「メディアを追加」ボタンを押すと次の画面が表示される。
ここで「ファイルをアップロード」を選択する。
![](http://i.gyazo.com/bc1cbf3355bc994c15f98c8780b80be7.png)

次にアップロードしたい画像を選択する(画像省略)。

###ii) 成功すると、メディアライブラリに画像が追加されるので追加したい画像を選んで挿入する
![](http://i.gyazo.com/e0b46f7ccc6f4ee0eaeeb054fe4d8e39.png)

# ブログ記事を編集・削除する
##1 投稿管理画面で「編集」または「ゴミ箱」を選ぶ
![](http://i.gyazo.com/47c495f6c1f296c222ccc12c2466bf36.png)

# プロフィールや経歴を編集する
##1 メニューから「固定ページ」を選択
![](http://puu.sh/auz7p/8270e70898.png)

##2 ブログと同じ要領でプロフィールと経歴をそれぞれ編集する
![](http://puu.sh/auznI/c8a9b7d42d.png)
